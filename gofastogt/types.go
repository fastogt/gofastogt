package gofastogt

import (
	"strings"
	"sync/atomic"
	"time"
)

type DurationMsec int64
type UtcTimeMsec int64

type PlatformType string

const (
	WEB      = "web"
	ANDROID  = "android"
	IOS      = "ios"
	WINDOWS  = "windows"
	LINUX    = "linux"
	MACOSX   = "macosx"
	APPLE_TV = "apple_tv"
	ROKU     = "roku"
	WEBOS    = "webos"
	TIZEN    = "tizen"
	UNKNOWN  = "unknown"
)

func MakePlatformTypeFromString(data string) PlatformType {
	lower := strings.ToLower(data)
	if lower == WEB {
		return WEB
	} else if lower == ANDROID {
		return ANDROID
	} else if lower == IOS {
		return IOS
	} else if lower == WINDOWS {
		return WINDOWS
	} else if lower == LINUX {
		return LINUX
	} else if lower == MACOSX || lower == "darwin" {
		return MACOSX
	} else if lower == APPLE_TV {
		return APPLE_TV
	} else if lower == ROKU {
		return ROKU
	} else if lower == WEBOS {
		return WEBOS
	} else if lower == TIZEN {
		return TIZEN
	}
	return UNKNOWN
}

func MakeUTCTimestamp() UtcTimeMsec {
	return Time2UtcTimeMsec(time.Now())
}

func Time2UtcTimeMsec(time time.Time) UtcTimeMsec {
	return UtcTimeMsec(time.UnixNano() / 1e6)
}

func UtcTime2Time(msec UtcTimeMsec) time.Time {
	return time.Unix(int64(msec)/1e3, (int64(msec)%1e3)*1e6)
}

type Point struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type Rational struct {
	Num int `json:"num"`
	Den int `json:"den"`
}

type Size struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

type UniqueIDU64 struct {
	counter uint64
}

func (c *UniqueIDU64) Get() uint64 {
	for {
		val := atomic.LoadUint64(&c.counter)
		if atomic.CompareAndSwapUint64(&c.counter, val, val+1) {
			return val
		}
	}
}

// Max returns the larger of x or y.
func MaxInt(x, y int) int {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func MinInt(x, y int) int {
	if x > y {
		return y
	}
	return x
}

// Max returns the larger of x or y.
func MaxInt64(x, y int64) int64 {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func MinInt64(x, y int64) int64 {
	if x > y {
		return y
	}
	return x
}
