package gofastogt

import (
	"os"
)

// Recreates or opens the file if it exists
// Recreates the log file if it is larger than the specified size
func InitLogFile(path string, fileMaxSizeBytes int64) (*os.File, error) {
	fInfo, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
		}
		return nil, err
	}
	if fInfo.Size() > fileMaxSizeBytes {
		if err = os.Remove(path); err != nil {
			return nil, err
		}
	}
	return os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
}
