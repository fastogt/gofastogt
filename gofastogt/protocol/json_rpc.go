package protocol

import (
	"encoding/json"
	"fmt"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

const kJsonRPCVersion = "2.0"

type wireVersionTag struct{}

func (wireVersionTag) MarshalJSON() ([]byte, error) {
	return json.Marshal(kJsonRPCVersion)
}

func (wireVersionTag) UnmarshalJSON(data []byte) error {
	version := ""
	if err := json.Unmarshal(data, &version); err != nil {
		return err
	}
	if version != kJsonRPCVersion {
		return fmt.Errorf("invalid RPC version %v", version)
	}
	return nil
}

type RPCId struct {
	name   string
	number uint64
}

func NewIntRPCId(v uint64) *RPCId    { return &RPCId{number: v} }
func NewStringRPCId(v string) *RPCId { return &RPCId{name: v} }

func NewSequenceId(id uint64) *RPCId {
	return NewStringRPCId(fmt.Sprintf("%016x", id))
}

func (id *RPCId) MarshalJSON() ([]byte, error) {
	if id.name != "" {
		return json.Marshal(id.name)
	}
	return json.Marshal(id.number)
}

func (id *RPCId) UnmarshalJSON(data []byte) error {
	*id = RPCId{}
	if err := json.Unmarshal(data, &id.number); err == nil {
		return nil
	}
	return json.Unmarshal(data, &id.name)
}

type Request struct {
	VersionTag wireVersionTag   `json:"jsonrpc"`
	Id         *RPCId           `json:"id,omitempty"`
	Method     string           `json:"method"`
	Params     *json.RawMessage `json:"params,omitempty"`
}

func NewRequest(id *RPCId, method string, params *json.RawMessage) *Request {
	request := Request{VersionTag: wireVersionTag{}, Id: id, Method: method, Params: params}
	return &request
}

func NewNotification(method string, params *json.RawMessage) *Request {
	request := Request{VersionTag: wireVersionTag{}, Id: nil, Method: method, Params: params}
	return &request
}

func (request *Request) IsValid() bool {
	return request.Method != ""
}

func (request *Request) IsNotification() bool {
	return request.Id == nil
}

func (request *Request) ToBytes() ([]byte, error) {
	return json.Marshal(request)
}

// Json Response RPC class

type RPCError struct {
	gofastogt.ErrorJson
	Data *json.RawMessage `json:"data,omitempty"`
}

func (rpc *RPCError) UnmarshalJSON(data []byte) error {
	err := rpc.ErrorJson.UnmarshalJSON(data)
	if err != nil {
		return err
	}

	optional := struct {
		Data *json.RawMessage `json:"data,omitempty"`
	}{}
	err = json.Unmarshal(data, &optional)
	if err != nil {
		return err
	}

	rpc.Data = optional.Data
	return nil
}

func (err *RPCError) Error() string {
	return err.Message
}

type Response struct {
	VersionTag wireVersionTag   `json:"jsonrpc"`
	Result     *json.RawMessage `json:"result,omitempty"`
	Error      *RPCError        `json:"error,omitempty"`
	Id         *RPCId           `json:"id,omitempty"`
}

func (response *Response) IsValid() bool {
	return response.Id != nil
}

func (response *Response) IsError() bool {
	return response.Error != nil
}

func (response *Response) IsMessage() bool {
	return response.Result != nil
}

func (response *Response) ToBytes() ([]byte, error) {
	return json.Marshal(response)
}

func NewResponseMessage(id *RPCId, result *json.RawMessage) *Response {
	response := Response{Id: id, Result: result}
	return &response
}

func NewRPCError(code int, message string) *RPCError {
	return NewRPCErrorWithData(code, message, nil)
}

func NewRPCErrorWithData(code int, message string, data *json.RawMessage) *RPCError {
	js := gofastogt.ErrorJson{Code: code, Message: message}
	return &RPCError{js, data}
}

func NewResponseError(id *RPCId, err *RPCError) *Response {
	request := Response{Id: id, Result: nil, Error: err}
	return &request
}

func ParseResponseOrRequest(b []byte) (*Request, *Response) {
	var parsedRequest Request
	if err := json.Unmarshal(b, &parsedRequest); err == nil {
		if parsedRequest.IsValid() {
			return &parsedRequest, nil
		}
	}

	var parsedResponse Response
	if err := json.Unmarshal(b, &parsedResponse); err == nil {
		if parsedResponse.IsValid() {
			return nil, &parsedResponse
		}
	}

	return nil, nil
}

// error response

func NewErrorResponse(err RPCError) *gofastogt.ErrorResponse {
	return gofastogt.NewErrorResponse(err.ErrorJson)
}
