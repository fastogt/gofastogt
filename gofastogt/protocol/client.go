package protocol

import (
	"encoding/binary"
	"encoding/json"
	"io"
	"net"
	"strconv"
	"sync"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type IClientObserver interface {
	OnClientStateChanged(status ConnectionStatus)
	ProcessRequest(request *Request)
	ProcessResponse(request *Request, response *Response)
}

type RequestCallbackT func(*Request, *Response)

type RequestSaveEntry struct {
	request  *Request
	callback RequestCallbackT
}

type Client struct {
	conn       *net.TCPConn
	status     ConnectionStatus
	compressor ICompressor

	host          gofastogt.HostAndPort
	mutex         sync.Mutex
	requestsQueue map[RPCId]RequestSaveEntry

	// observer
	Observer IClientObserver
}

func NewGzipClient(host gofastogt.HostAndPort, observer IClientObserver) *Client {
	return &Client{host: host, Observer: observer, status: INIT, compressor: NewGZipCompressor(), conn: nil, requestsQueue: nil, mutex: sync.Mutex{}}
}

func (client *Client) SetStatus(status ConnectionStatus) {
	client.status = status
	if client.Observer != nil {
		client.Observer.OnClientStateChanged(status)
	}
}

func (client *Client) GetStatus() ConnectionStatus {
	return client.status
}

func (client *Client) IsConnected() bool {
	return client.status != INIT
}

func (client *Client) Connect() error {
	portInt := strconv.FormatInt(int64(client.host.Port), 10)
	tcpAddr, err := net.ResolveTCPAddr("tcp", net.JoinHostPort(client.host.Host, portInt))
	if err != nil {
		return err
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return err
	}

	client.conn = conn
	client.SetStatus(CONNECTED)
	client.mutex.Lock()
	client.requestsQueue = map[RPCId]RequestSaveEntry{}
	client.mutex.Unlock()
	return nil
}

func (client *Client) Disconnect() error {
	if client.status == INIT {
		return nil
	}

	if err := client.conn.Close(); err != nil {
		return err
	}

	client.flush()
	return nil
}

func (client *Client) flush() {
	client.conn = nil
	client.SetStatus(INIT)
	client.mutex.Lock()
	client.requestsQueue = map[RPCId]RequestSaveEntry{}
	client.mutex.Unlock()
}

func (client *Client) SendRequest(request *Request) (*RPCId, error) {
	return client.SendRequestWithCallback(request, nil)
}

func (client *Client) SendRequestWithCallback(request *Request, callback RequestCallbackT) (*RPCId, error) {
	if !client.IsConnected() {
		return nil, gofastogt.ErrNotConnected
	}

	commandId := *request.Id

	data, err := request.ToBytes()
	if err != nil {
		return nil, err
	}

	client.mutex.Lock()
	err = client.write(data)
	if err != nil {
		client.mutex.Unlock()
		return nil, err
	}

	if !request.IsNotification() {
		client.requestsQueue[commandId] = RequestSaveEntry{request, callback}
	}
	client.mutex.Unlock()

	return &commandId, nil
}

func (client *Client) SendResponse(cid *RPCId, params *json.RawMessage) error {
	response := NewResponseMessage(cid, params)
	return client.SendResponseRaw(response)
}

func (client *Client) SendResponseError(cid *RPCId, err *RPCError) error {
	response := NewResponseError(cid, err)
	return client.SendResponseRaw(response)
}

func (client *Client) SendResponseRaw(response *Response) error {
	if !client.IsConnected() {
		return gofastogt.ErrNotConnected
	}

	data, err := response.ToBytes()
	if err != nil {
		return err
	}

	err = client.write(data)
	if err != nil {
		return err
	}

	return nil
}

func (client *Client) ReadCommand() ([]byte, error) {
	if !client.IsConnected() {
		return nil, gofastogt.ErrNotConnected
	}

	packetLength, err := readDataSize(client.conn)
	if err != nil {
		return nil, err
	}

	data, err := readData(client.conn, packetLength)
	if err != nil {
		return nil, err
	}

	data, err = client.compressor.Decode(data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (client *Client) DecodeResponseOrRequest(data []byte) (*Request, *Response) {
	return ParseResponseOrRequest(data)
}

func (client *Client) PopRequest(cid RPCId) (*Request, RequestCallbackT) {
	client.mutex.Lock()
	if req, ok := client.requestsQueue[cid]; ok {
		delete(client.requestsQueue, cid)
		client.mutex.Unlock()
		return req.request, req.callback
	}
	client.mutex.Unlock()
	return nil, nil
}

// private:
func readDataSize(conn *net.TCPConn) (uint32, error) {
	header := make([]byte, 4)
	_, err := io.ReadFull(conn, header)
	if err != nil {
		return 0, err
	}

	packetLength := binary.BigEndian.Uint32(header)
	return packetLength, nil
}

func readData(conn *net.TCPConn, size uint32) ([]byte, error) {
	data := make([]byte, size)
	_, err := io.ReadFull(conn, data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (client *Client) write(data []byte) error {
	if !client.IsConnected() {
		return gofastogt.ErrNotConnected
	}

	data, err := client.compressor.Encode(data)
	if err != nil {
		return err
	}

	length := len(data)
	packedLength := make([]byte, 4)
	binary.BigEndian.PutUint32(packedLength, uint32(length))

	_, err = client.conn.Write(packedLength)
	if err != nil {
		return err
	}

	_, err = client.conn.Write(data)
	if err != nil {
		return err
	}
	return nil
}
