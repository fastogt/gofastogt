package protocol

import (
	"bytes"
	"compress/gzip"
)

type ICompressor interface {
	Encode(bytes []byte) ([]byte, error)
	Decode(bytes []byte) ([]byte, error)
}

type RawCompressor struct {
}

func (raw *RawCompressor) Encode(data []byte) ([]byte, error) {
	return data, nil
}

func (raw *RawCompressor) Decode(data []byte) ([]byte, error) {
	return data, nil
}

type GZipCompressor struct {
	reader *gzip.Reader
}

func NewGZipCompressor() *GZipCompressor {
	p := new(GZipCompressor)
	p.reader = new(gzip.Reader)
	return p
}

func (compressor *GZipCompressor) Encode(data []byte) ([]byte, error) {
	var b bytes.Buffer
	w := gzip.NewWriter(&b)
	if _, err := w.Write(data); err != nil {
		return nil, err
	}

	if err := w.Close(); err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}

func (compressor *GZipCompressor) Decode(data []byte) ([]byte, error) {
	r := bytes.NewBuffer(data)
	if err := compressor.reader.Reset(r); err != nil {
		return nil, err
	}

	var result bytes.Buffer
	if _, err := result.ReadFrom(compressor.reader); err != nil {
		return nil, err
	}

	if err := compressor.reader.Close(); err != nil {
		return nil, err
	}

	return result.Bytes(), nil
}
