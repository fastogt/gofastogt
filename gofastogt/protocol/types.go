package protocol

type ConnectionStatus int

const (
	INIT ConnectionStatus = iota
	CONNECTED
)

func (s ConnectionStatus) IsValid() bool {
	switch s {
	case INIT, CONNECTED:
		return true
	}
	return false
}
