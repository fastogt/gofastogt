package gofastogt

import (
	"crypto/md5"
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/fastogt/gofastogt/gofastogt/cpuid"
	"gitlab.com/fastogt/gofastogt/gofastogt/disk"
	"gitlab.com/fastogt/gofastogt/gofastogt/machineid"
)

type ExpiredKey []byte
type HardwareHash []byte
type AlgoType int

func (hw HardwareHash) IsValid() bool {
	return len(hw) == LEN_HARDWARE_KEY
}

const (
	HDD AlgoType = iota
	MACHINE_ID
)

const LEN_EXP_KEY = 97
const LEN_HARDWARE_KEY = 65
const CHUNK_SIZE = 8

var ErrInvalidKey = errors.New("invalid key")

type LicenseKey string

func (license LicenseKey) IsValid() bool {
	return len(license) == LEN_EXP_KEY
}

func makeHardwareHash() HardwareHash {
	var hash [LEN_HARDWARE_KEY]byte
	return hash[:]
}

func getExpireTimeFromKey(key LicenseKey, sameMachine bool, project string) (*time.Time, error) {
	exp_key := []byte(key)
	hash, err := GetHardwareHash(exp_key)
	if err != nil {
		return nil, err
	}

	if !IsValidHardwareHashFull(hash, sameMachine) {
		return nil, ErrInvalidKey
	}

	var crc uint64
	for i := 0; i < len(project); i++ {
		crc += uint64(project[i])
	}

	var res_rune, project_rune []byte
	for i := 0; i < CHUNK_SIZE; i++ {
		res_rune = append(res_rune, exp_key[i*12], exp_key[i*12+11])
		project_rune = append(project_rune, exp_key[i*12+1], exp_key[i*12+10])
	}

	project_calc, err := strconv.ParseUint(string(project_rune), 16, 64)
	if err != nil {
		return nil, ErrInvalidKey
	}

	exp_time_calc, err := strconv.ParseUint(string(res_rune), 16, 64)
	if err != nil {
		return nil, ErrInvalidKey
	}

	//compare project
	if crc != project_calc {
		return nil, ErrInvalidKey
	}

	exp_time := UtcTime2Time(UtcTimeMsec(exp_time_calc))
	return &exp_time, nil
}

func GenerateHardwareHash(algo AlgoType) (HardwareHash, error) {
	getMD5Hash := func(text string) string {
		hash := md5.Sum([]byte(text))
		return ToStringHexFromBytes(hash[:])
	}

	eax, ebx, ecx, edx := cpuid.CpuID(0)

	data := fmt.Sprintf("%08X %08X %08X %08X", eax, ebx, ecx, edx)
	cpuID := getMD5Hash(data)
	lhash := makeHardwareHash()
	if algo == HDD {
		lhash[LEN_HARDWARE_KEY-1] = '0'
		serial, err := disk.GetDiskSerialNumber("/")
		if err != nil {
			return nil, err
		}

		hdd := getMD5Hash(*serial)
		for i := 0; i < LEN_HARDWARE_KEY/2; i++ {
			lhash[i*2] = cpuID[i]
			lhash[i*2+1] = hdd[i]
		}
		return lhash, nil
	} else if algo == MACHINE_ID {
		lhash[LEN_HARDWARE_KEY-1] = '1'
		mid, err := machineid.MachineID()
		if err != nil {
			return nil, err
		}

		hdd := getMD5Hash(*mid)
		for i := 0; i < LEN_HARDWARE_KEY/2; i++ {
			lhash[i*2] = cpuID[i]
			lhash[i*2+1] = hdd[i]
		}
		return lhash, nil
	}
	return nil, errors.New("invalid algorithm type")
}

func GetHardwareHash(exp_key ExpiredKey) (HardwareHash, error) {
	if len(exp_key) != LEN_EXP_KEY {
		return nil, ErrInvalidKey
	}

	lhash := makeHardwareHash()
	for i := 0; i < CHUNK_SIZE; i++ {
		lhash[i*CHUNK_SIZE] = exp_key[i*12+2]
		lhash[i*CHUNK_SIZE+1] = exp_key[i*12+3]
		lhash[i*CHUNK_SIZE+2] = exp_key[i*12+4]
		lhash[i*CHUNK_SIZE+3] = exp_key[i*12+5]
		lhash[i*CHUNK_SIZE+4] = exp_key[i*12+6]
		lhash[i*CHUNK_SIZE+5] = exp_key[i*12+7]
		lhash[i*CHUNK_SIZE+6] = exp_key[i*12+8]
		lhash[i*CHUNK_SIZE+7] = exp_key[i*12+9]
	}
	lhash[len(lhash)-1] = exp_key[len(exp_key)-1]
	return lhash, nil
}

func IsValidHardwareHashFull(hash HardwareHash, sameMachine bool) bool {
	if len(hash) != LEN_HARDWARE_KEY {
		return false
	}

	algo := hash[len(hash)-1]
	algoType, err := strconv.Atoi(string(algo))
	if err != nil {
		return false
	}

	lic, err := GenerateHardwareHash(AlgoType(algoType))
	if err != nil {
		return false
	}

	if !sameMachine {
		return true
	}

	compare := func(a, b HardwareHash) bool {
		if len(a) != len(b) {
			return false
		}

		for i, v := range a {
			if v != b[i] {
				return false
			}
		}
		return true
	}
	return compare(lic, hash)
}

type License struct {
	tm time.Time
}

func NewLicense(msec UtcTimeMsec) *License {
	return &License{tm: UtcTime2Time(msec)}
}

func (license *License) UpdateLicense(key *LicenseKey, sameMachine bool, project string) time.Time {
	if key == nil {
		zero := UtcTime2Time(0)
		license.tm = zero
		return license.GetTM()
	}

	tm, err := getExpireTimeFromKey(*key, sameMachine, project)
	if err != nil {
		zero := UtcTime2Time(0)
		license.tm = zero
		return license.GetTM()
	}

	license.tm = *tm
	return license.GetTM()
}

func (license *License) IsExpired() bool {
	return license.tm.Before(time.Now())
}

func (license *License) GetTM() time.Time {
	return license.tm
}
