package gofastogt

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

const kLocalhostText = "localhost"
const kLocalhostDigits = "127.0.0.1"
const kLocalhostIPV6Text = "::1"
const kLocalhostIPV6Digits = "::ffff:127.0.0.1"

const kDefaultRouteDigits = "0.0.0.0"
const kDefaultRouteIPV6Digits = "::"

type IPAddress struct {
	IP string `yaml:"ip" json:"ip"`
}

func (ip *IPAddress) UnmarshalJSON(data []byte) error {
	required := struct {
		IP *string `json:"ip"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.IP == nil {
		return errors.New("ip field required")
	}
	if len(*required.IP) == 0 {
		return errors.New("invalid ip")
	}

	ip.IP = *required.IP
	return nil
}

type HttpsConfig struct {
	Key  string `yaml:"key"  json:"key"`
	Cert string `yaml:"cert" json:"cert"`
}

func (https *HttpsConfig) UnmarshalJSON(data []byte) error {
	required := struct {
		Key  *string `json:"key"`
		Cert *string `json:"cert"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}
	if required.Key == nil {
		return errors.New("key field required")
	}
	if len(*required.Key) == 0 {
		return errors.New("invalid key")
	}

	if required.Cert == nil {
		return errors.New("cert field required")
	}
	if len(*required.Cert) == 0 {
		return errors.New("invalid cert")
	}

	https.Key = *required.Key
	https.Cert = *required.Cert
	return nil
}

type HostAndPort struct {
	Host string `json:"host"`
	Port uint16 `json:"port"`
}

func (host *HostAndPort) UnmarshalJSON(data []byte) error {
	required := struct {
		Host *string `json:"host"`
		Port *uint16 `json:"port"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}
	if required.Host == nil {
		return errors.New("host field required")
	}
	if len(*required.Host) == 0 {
		return errors.New("invalid host")
	}

	if required.Port == nil {
		return errors.New("cert field required")
	}

	host.Host = *required.Host
	host.Port = *required.Port
	return nil
}

func MakeHostAndPortFromString(data string) (*HostAndPort, error) {
	parts := strings.Split(data, ":")
	if len(parts) < 2 {
		return nil, ErrInvalidInput
	}

	port, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, err
	}

	if !IsIPOrHost(parts[0]) {
		return nil, ErrInvalidInput
	}
	return &HostAndPort{parts[0], uint16(port)}, nil
}

func MakeHostAndPortFromUrl(data string) (*HostAndPort, error) {
	parts := strings.Split(data, ":")
	if len(parts) < 2 {
		return nil, ErrInvalidInput
	}

	var host string
	var port string
	if len(parts) == 3 {
		port = parts[2]
	}

	if len(parts) == 2 {
		if parts[0] == "https" {
			port = "443"
		}

		if parts[0] == "http" {
			port = "80"
		}
	}

	host = strings.ReplaceAll(parts[1], "/", "")

	portInt, err := strconv.Atoi(port)
	if err != nil {
		return nil, err
	}

	return &HostAndPort{host, uint16(portInt)}, nil
}

func IsLocalHost(host string) bool {
	if host == "" {
		return false
	}

	return host == kLocalhostText || host == kLocalhostDigits || host == kLocalhostIPV6Text ||
		host == kLocalhostIPV6Digits
}

func IsDefaultRoute(host string) bool {
	if host == "" {
		return false
	}

	if host == kDefaultRouteDigits {
		return true
	}

	// There are multiple ways to write IPv6 addresses.
	// We're looking for any representation of the address "0:0:0:0:0:0:0:0".
	// A single sequence of "0" bytes in an IPv6 address may be represented as "::",
	// so we must also match addresses like "::" or "0::0:0".
	// Return false if a character other than ':' or '0' is contained in the address.
	for _, c := range host {
		if c != ':' && c != '0' && c != '[' && c != ']' {
			return false
		}
	}
	return true
}

func NewLocalHostIPV4(port uint16) *HostAndPort {
	return &HostAndPort{kLocalhostDigits, port}
}

func NewLocalHostIPV6(port uint16) *HostAndPort {
	return &HostAndPort{kLocalhostIPV6Text, port}
}

func NewDefaultRouteIPV4(port uint16) *HostAndPort {
	return &HostAndPort{kDefaultRouteDigits, port}
}

func NewDefaultRouteIPV6(port uint16) *HostAndPort {
	return &HostAndPort{kDefaultRouteIPV6Digits, port}
}

func (host *HostAndPort) IsDefaultRoute() bool {
	return IsDefaultRoute(host.Host)
}

func (host *HostAndPort) IsLocalHost() bool {
	return IsLocalHost(host.Host)
}

func (host *HostAndPort) String() string {
	return fmt.Sprintf("%s:%d", host.Host, host.Port)
}

func GetIPFromRequest(r *http.Request) (*string, *uint16, error) {
	if r == nil {
		return nil, nil, ErrInvalidInput
	}

	IPAddress := r.Header.Get("X-Real-Ip")
	if netIP := net.ParseIP(IPAddress); netIP != nil {
		return &IPAddress, nil, nil
	}

	IPAddress = r.Header.Get("X-Forwarded-For")
	addrs := strings.Split(IPAddress, ",")
	for _, ip := range addrs {
		if netIP := net.ParseIP(ip); netIP != nil {
			return &ip, nil, nil
		}
	}
	IPAddress, p, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return nil, nil, err
	}
	if netIP := net.ParseIP(IPAddress); netIP != nil {
		p, err := strconv.Atoi(p)
		if err != nil {
			return nil, nil, err
		}
		port := uint16(p)
		return &IPAddress, &port, nil
	}
	return nil, nil, ErrInvalidInput
}

func IsHost(host string) bool {
	switch {
	case len(host) == 0:
		return false
	case len(host) > 255:
		return false
	}
	var l int
	for i := 0; i < len(host); i++ {
		b := host[i]
		if b == '.' {
			// check domain labels validity
			switch {
			case i == l:
				return false
			case i-l > 63:
				return false
			case host[l] == '-':
				return false
			case host[i-1] == '-':
				return false
			}
			l = i + 1
			continue
		}
		// test label character validity, note: tests are ordered by decreasing validity frequency
		if !(b >= 'a' && b <= 'z' || b >= '0' && b <= '9' || b == '-' || b >= 'A' && b <= 'Z') {
			c, _ := utf8.DecodeRuneInString(host[i:])
			if c == utf8.RuneError {
				return false
			}
			return false
		}
	}
	// check top level domain validity
	switch {
	case l == len(host):
		return false
	case len(host)-l > 63:
		return false
	case host[l] == '-':
		return false
	case host[len(host)-1] == '-':
		return false
	case host[l] >= '0' && host[l] <= '9':
		return false
	}
	return true
}

func IsIP(host string) bool {
	return net.ParseIP(host) != nil
}

func IsIPOrHost(host string) bool {
	return IsIP(host) || IsHost(host)
}

func IsValidUrl(url string, timeExpire time.Duration) bool {
	if len(url) == 0 {
		return false
	}

	client := http.Client{Timeout: timeExpire}
	resp, err := client.Head(url)
	if err != nil {
		return false
	}

	return resp.StatusCode == 200
}

func ToBytesFromStringHex(text string) ([]byte, error) {
	return hex.DecodeString(text)
}

func ToStringHexFromBytes(bytes []byte) string {
	return hex.EncodeToString(bytes)
}
