package gofastogt

import (
	"encoding/json"
	"os"
	"strings"
)

type IBytesConverter interface {
	ToBytes() (json.RawMessage, error)
}

func IsRunningInDocker() bool {
	_, err := os.Stat("/proc/1/cgroup")
	if err != nil {
		return false
	}
	file, err := os.ReadFile("/proc/1/cgroup")
	if err != nil {
		return false
	}
	return strings.Contains(string(file), "docker")
}
