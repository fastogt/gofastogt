package gofastogt

import (
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

func preparePath(path string) (*string, error) {
	if len(path) == 0 || (path[0] != '~' && path[0] != '/') {
		return nil, ErrInvalidInput
	}
	if path[0] == '/' {
		return &path, nil
	}
	usr, err := user.Current()
	if err != nil {
		return nil, err
	}
	path = filepath.Join(usr.HomeDir, path[1:])
	return &path, nil
}

func StableFilePath(path string) (*string, error) {
	return preparePath(path)
}

func StableDirPath(path string) (*string, error) {
	stableDir, err := preparePath(path)
	if err != nil {
		return nil, err
	}

	if (*stableDir)[len(*stableDir)-1] != '/' {
		*stableDir += "/"
	}
	return stableDir, nil
}

type FilePathProtocol struct {
	path string
}

func MakeFilePathProtocol(path string) (*FilePathProtocol, error) {
	stabled, err := StableFilePath(path)
	if err != nil {
		return nil, err
	}

	return &FilePathProtocol{*stabled}, nil
}

func (file *FilePathProtocol) GetFilePath() string {
	return "file://" + file.path
}

func (file *FilePathProtocol) GetPath() string {
	return file.path
}

func ScanFolder(path string, extensions []string, recursive bool) ([]string, error) {
	items, err := os.ReadDir(path)
	if err != nil {
		return []string{}, err
	}

	files := make([]string, 0)

	for _, item := range items {
		itemPath := filepath.Join(path, item.Name())
		if item.IsDir() {
			if recursive {
				subFiles, err := ScanFolder(itemPath, extensions, recursive)
				if err != nil {
					return []string{}, err
				}
				files = append(files, subFiles...)
			}
		} else {
			if HasValidExtension(itemPath, extensions) {
				files = append(files, itemPath)
			}
		}
	}
	return files, nil
}

func HasValidExtension(path string, extensions []string) bool {
	ext := strings.TrimPrefix(filepath.Ext(path), ".")
	ext = strings.ToLower(ext)
	for _, validExt := range extensions {
		if ext == validExt {
			return true
		}
	}
	return false
}
