package gofastogt

import (
	"encoding/json"
	"errors"
)

var ErrInvalidInput = errors.New("invalid input")
var ErrNotConnected = errors.New("not connected")
var ErrInvalidFormat = errors.New("invalid m3u file format")

// data field optional

type ErrorJson struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (js *ErrorJson) Error() error {
	return errors.New(js.Message)
}

func (js *ErrorJson) UnmarshalJSON(data []byte) error {
	required := struct {
		Code    *int    `json:"code"`
		Message *string `json:"message"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Code == nil {
		return errors.New("code field required")
	}

	if required.Message == nil {
		return errors.New("message field required")
	}

	js.Code = *required.Code
	js.Message = *required.Message
	return nil
}
