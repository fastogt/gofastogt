package gofastogt

import (
	"encoding/json"
	"errors"
)

type OperationSystem struct {
	Name     PlatformType `json:"name"`
	Version  string       `json:"version"`
	Arch     string       `json:"arch"`
	RamTotal uint64       `json:"ram_total"`
	RamFree  uint64       `json:"ram_free"`
}

func (operation *OperationSystem) UnmarshalJSON(data []byte) error {
	required := struct {
		Name     *string `json:"name"`
		Version  *string `json:"version"`
		Arch     *string `json:"arch"`
		RamTotal *uint64 `json:"ram_total"`
		RamFree  *uint64 `json:"ram_free"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Name == nil {
		return errors.New("name field required")
	}
	if len(*required.Name) == 0 {
		return errors.New("invalid name")
	}

	if required.Version == nil {
		return errors.New("version field required")
	}
	if len(*required.Version) == 0 {
		return errors.New("invalid version")
	}

	if required.Arch == nil {
		return errors.New("arch field required")
	}
	if len(*required.Arch) == 0 {
		return errors.New("invalid arch")
	}

	if required.RamTotal == nil {
		return errors.New("ram_total field required")
	}
	if required.RamFree == nil {
		return errors.New("ram_free field required")
	}

	operation.Name = MakePlatformTypeFromString(*required.Name)
	operation.Version = *required.Version
	operation.Arch = *required.Arch
	operation.RamFree = *required.RamFree
	operation.RamTotal = *required.RamTotal
	return nil
}
