package gofastogt

type ProjectInfo struct {
	Project string `json:"project"`
	Version string `json:"version"`
}

func MakeProjectInfo(project string, version string) ProjectInfo {
	return ProjectInfo{Project: project, Version: version}
}

type LicensedProjectInfo struct {
	ProjectInfo
	ExpireTime UtcTimeMsec `json:"expiration_time"`
}

func MakeLicensedProjectInfo(project ProjectInfo, expTime UtcTimeMsec) LicensedProjectInfo {
	return LicensedProjectInfo{ProjectInfo: project, ExpireTime: expTime}
}

type ProjectVersion struct {
	Short         string `json:"short"`
	HumanReadable string `json:"human_readable"`
	Version       int64  `json:"version"`
}
