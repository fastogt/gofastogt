package gofastogt

import (
	"encoding/json"
	"errors"
	"sort"
)

// bson for snapshots in crocott
type Machine struct {
	Cpu           float64     `bson:"cpu"             json:"cpu"`
	Gpu           float64     `bson:"gpu"             json:"gpu"`
	LoadAverage   string      `bson:"load_average"    json:"load_average"`
	MemoryTotal   uint64      `bson:"memory_total"    json:"memory_total"`
	MemoryFree    uint64      `bson:"memory_free"     json:"memory_free"`
	HddTotal      uint64      `bson:"hdd_total"       json:"hdd_total"`
	HddFree       uint64      `bson:"hdd_free"        json:"hdd_free"`
	BandwidthIn   uint64      `bson:"bandwidth_in"    json:"bandwidth_in"`
	BandwidthOut  uint64      `bson:"bandwidth_out"   json:"bandwidth_out"`
	Uptime        UtcTimeMsec `bson:"uptime"          json:"uptime"`
	Timestamp     UtcTimeMsec `bson:"timestamp"       json:"timestamp"`
	TotalBytesIn  uint64      `bson:"total_bytes_in"  json:"total_bytes_in"`
	TotalBytesOut uint64      `bson:"total_bytes_out" json:"total_bytes_out"`
}

func (machine *Machine) UnmarshalJSON(data []byte) error {
	req := struct {
		Cpu           *float64     `json:"cpu"`
		Gpu           *float64     `json:"gpu"`
		LoadAverage   *string      `json:"load_average"`
		MemoryTotal   *uint64      `json:"memory_total"`
		MemoryFree    *uint64      `json:"memory_free"`
		HddTotal      *uint64      `json:"hdd_total"`
		HddFree       *uint64      `json:"hdd_free"`
		BandwidthIn   *uint64      `json:"bandwidth_in"`
		BandwidthOut  *uint64      `json:"bandwidth_out"`
		Uptime        *UtcTimeMsec `json:"uptime"`
		Timestamp     *UtcTimeMsec `json:"timestamp"`
		TotalBytesIn  *uint64      `json:"total_bytes_in"`
		TotalBytesOut *uint64      `json:"total_bytes_out"`
	}{}

	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}

	if req.Cpu == nil {
		return errors.New("cpu field required")
	}
	if req.Gpu == nil {
		return errors.New("gpu field required")
	}
	if req.LoadAverage == nil {
		return errors.New("load_average field required")
	}
	if req.MemoryTotal == nil {
		return errors.New("memory_total field required")
	}
	if req.MemoryFree == nil {
		return errors.New("memory_free field required")
	}
	if req.HddTotal == nil {
		return errors.New("hdd_total field required")
	}
	if req.HddFree == nil {
		return errors.New("hdd_free field required")
	}
	if req.BandwidthIn == nil {
		return errors.New("bandwidth_in field required")
	}
	if req.BandwidthOut == nil {
		return errors.New("bandwidth_out field required")
	}
	if req.Uptime == nil {
		return errors.New("uptime field required")
	}
	if req.Timestamp == nil {
		return errors.New("timestamp field required")
	}
	if req.TotalBytesIn == nil {
		return errors.New("total_bytes_in field required")
	}
	if req.TotalBytesOut == nil {
		return errors.New("total_bytes_out field required")
	}

	machine.Cpu = *req.Cpu
	machine.Gpu = *req.Gpu
	machine.LoadAverage = *req.LoadAverage
	machine.MemoryTotal = *req.MemoryTotal
	machine.MemoryFree = *req.MemoryFree
	machine.HddTotal = *req.HddTotal
	machine.HddFree = *req.HddFree
	machine.BandwidthIn = *req.BandwidthIn
	machine.BandwidthOut = *req.BandwidthOut
	machine.Uptime = *req.Uptime
	machine.Timestamp = *req.Timestamp
	machine.TotalBytesIn = *req.TotalBytesIn
	machine.TotalBytesOut = *req.TotalBytesOut

	return nil
}

func NewZeroMachine() *Machine {
	mach := MakeMachine(0, 0, "0 0 0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	return &mach
}

func MakeMachine(cpu float64, gpu float64, lav string, memt uint64, memf uint64, hddt uint64, hddf uint64,
	bandwIn uint64, bandwOut uint64, ts UtcTimeMsec, uptime UtcTimeMsec, tbytesIn uint64, tbytesOut uint64) Machine {
	return Machine{Cpu: cpu, Gpu: gpu, LoadAverage: lav, MemoryTotal: memt, MemoryFree: memf,
		HddTotal: hddt, HddFree: hddf, BandwidthIn: bandwIn, BandwidthOut: bandwOut,
		Uptime: uptime, Timestamp: ts,
		TotalBytesIn: tbytesIn, TotalBytesOut: tbytesOut}
}

func GetStoreBytes(stats []Machine, startTimeStamp UtcTimeMsec) (*float64, error) {
	bytes := 0.0
	if len(stats) == 0 {
		return &bytes, nil
	}

	index := sort.Search(len(stats), func(i int) bool { return stats[i].Timestamp >= startTimeStamp })

	if index != len(stats) {
		for i := index; i < len(stats); i++ {
			bytes += float64(stats[i].HddTotal - stats[i].HddFree)
		}
		bytes /= float64(len(stats) - index)
	}

	return &bytes, nil
}

func GetNetBytes(stats []Machine, startTimeStamp UtcTimeMsec) (*float64, error) {
	bytes := 0.0
	if len(stats) < 2 {
		return &bytes, nil
	}

	index := sort.Search(len(stats), func(i int) bool { return stats[i].Timestamp >= startTimeStamp })

	if index != len(stats) {
		first := stats[index]
		last := stats[len(stats)-1]
		bytes = float64(last.TotalBytesOut - first.TotalBytesOut)
	}

	return &bytes, nil
}
