package gofastogt

type ErrorResponse struct {
	Error ErrorJson `json:"error"`
}

func NewErrorResponse(err ErrorJson) *ErrorResponse {
	return &ErrorResponse{err}
}

type OkResponse struct {
	Data interface{} `json:"data"`
}

func NewOkResponse(data interface{}) *OkResponse {
	return &OkResponse{data}
}
