package gofastogt

import (
	"math/rand"
)

func generate(length int, symbols string) (*string, error) {
	if length > 0 {
		code := make([]byte, length)
		for i := 0; i < length; i++ {
			code[i] = symbols[rand.Intn(len(symbols))]
		}
		str := string(code)
		return &str, nil
	}
	return nil, ErrInvalidInput
}

func GenerateString(length int) (*string, error) {
	return generate(length, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
}

func GenerateDigitString(length int) (*string, error) {
	return generate(length, "0123456789")
}

func GenerateLetterString(length int) (*string, error) {
	return generate(length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
}
