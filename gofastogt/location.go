package gofastogt

import (
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
)

// bson for servers in fastoeye
type Location struct {
	Latitude   float64 `bson:"lat" json:"lat"`
	Longtitude float64 `bson:"lng" json:"lng"`
}

func NewLocation(lt float64, lg float64) *Location {
	plt, plg := processLatAndLng(lt, lg)
	loc := Location{Latitude: plt, Longtitude: plg}
	return &loc
}

func (l *Location) Equal(other *Location) bool {
	return l.Latitude == other.Latitude && l.Longtitude == other.Longtitude
}

func (l *Location) UnmarshalJSON(data []byte) error {
	req := struct {
		Latitude   *float64 `json:"lat"`
		Longtitude *float64 `json:"lng"`
	}{}

	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}

	if req.Latitude == nil {
		return errors.New("lat field required")
	}
	if req.Longtitude == nil {
		return errors.New("lng field required")
	}

	plt, plg := processLatAndLng(*req.Latitude, *req.Longtitude)

	l.Latitude = plt
	l.Longtitude = plg

	return nil
}

func (l *Location) ParseFromString(location string) (Location, error) {
	loc := strings.Split(location, ",")
	lat, err := strconv.ParseFloat(loc[0], 64)
	if err != nil {
		return Location{}, err
	}

	lng, err := strconv.ParseFloat(loc[1], 64)
	if err != nil {
		return Location{}, err
	}

	return Location{Latitude: lat, Longtitude: lng}, nil
}

func (l *Location) String() string {
	parsedLatitude := strconv.FormatFloat(l.Latitude, 'f', -1, 64)
	parsedLongtitude := strconv.FormatFloat(l.Longtitude, 'f', -1, 64)

	return fmt.Sprintf("%s,%s", parsedLatitude, parsedLongtitude)
}

func processLatAndLng(lt float64, lg float64) (float64, float64) {
	if lt < -90.0 {
		lt = -90.0
	} else if lt > 90.0 {
		lt = 90.0
	}

	if !(lg >= -180 && lg < 180) {
		lg = math.Mod(lg+180.0, 360.0) - 180.0
	}

	return lt, lg
}
