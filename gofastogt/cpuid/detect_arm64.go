//go:build arm64 && !gccgo && !noasm && !appengine
// +build arm64,!gccgo,!noasm,!appengine

package cpuid

func initCPU() {
	CpuID = func(uint32) (a, b, c, d uint32) { return 0, 0, 0, 0 }
}
