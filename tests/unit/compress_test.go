package unittests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	protocol "gitlab.com/fastogt/gofastogt/gofastogt/protocol"
)

func TestCompress(t *testing.T) {
	gzip := protocol.NewGZipCompressor()
	data := []byte("{\"timestamp\":123, \"METHOD\":\"call\"}")
	encoded, err := gzip.Encode(data)
	assert.True(t, err == nil)
	assert.True(t, encoded != nil)
	decoded, err := gzip.Decode(encoded)
	assert.True(t, err == nil)
	assert.True(t, decoded != nil)
	assert.Equal(t, data, decoded)

	data2 := []byte("{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}")
	encoded, err = gzip.Encode(data2)
	assert.True(t, err == nil)
	assert.True(t, encoded != nil)
	decoded, err = gzip.Decode(encoded)
	assert.True(t, err == nil)
	assert.True(t, decoded != nil)
	assert.Equal(t, data2, decoded)
}

func TestBigCompress(t *testing.T) {
	gzip := protocol.NewGZipCompressor()
	data := []byte("JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate. It is based on a subset of the JavaScript Programming Language Standard ECMA-262 3rd Edition - December 1999. JSON is a text format that is completely language independent but uses conventions that are familiar to programmers of the C-family of languages, including C, C++, C#, Java, JavaScript, Perl, Python, and many others. These properties make JSON an ideal data-interchange language.\n\nJSON is built on two structures:\n\nA collection of name/value pairs. In various languages, this is realized as an object, record, struct, dictionary, hash table, keyed list, or associative array.\nAn ordered list of values. In most languages, this is realized as an array, vector, list, or sequence.\nThese are universal data structures. Virtually all modern programming languages support them in one form or another. It makes sense that a data format that is interchangeable with programming languages also be based on these structures.")
	encoded, err := gzip.Encode(data)
	assert.True(t, err == nil)
	assert.True(t, encoded != nil)
	decoded, err := gzip.Decode(encoded)
	assert.True(t, err == nil)
	assert.True(t, decoded != nil)
	assert.Equal(t, data, decoded)
}
