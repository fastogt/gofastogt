package unittests

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	protocol "gitlab.com/fastogt/gofastogt/gofastogt/protocol"
)

func TestResponse(t *testing.T) {
	//Arrange
	result := json.RawMessage("{\"some_key\": \"some_value\"}")
	response := protocol.NewResponseMessage(protocol.NewStringRPCId("1213"), &result)

	var err = protocol.NewRPCError(202, "some_message")
	responseErr := protocol.NewResponseError(protocol.NewStringRPCId("1213"), err)

	//Assert
	assert.True(t, response.IsValid())
	assert.False(t, response.IsError())
	assert.True(t, response.IsMessage())
	assert.Equal(t, response.Result, &result)

	assert.True(t, responseErr.IsValid())
	assert.True(t, responseErr.IsError())
	assert.False(t, responseErr.IsMessage())
	assert.Nil(t, responseErr.Result)
}
