package unittests

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestUnmarshalJSON(t *testing.T) {
	var operation gofastogt.OperationSystem

	dataTest := []byte(`{"name":"linux","version":"2.0","arch":"amd","ram_total":123,"ram_free":123}`)
	err := json.Unmarshal(dataTest, &operation)
	assert.NoError(t, err)
	assert.Equal(t, operation.Name, gofastogt.MakePlatformTypeFromString("linux"))
	assert.Equal(t, operation.RamTotal, uint64(123))
	assert.Equal(t, operation.RamFree, uint64(123))

	//  test name by nil
	dataTest2 := []byte(`{"ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest2, &operation)
	assert.Error(t, err)

	// test name by empty
	dataTest3 := []byte(`{"name":"","version":"2.0","arch":"user","ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest3, &operation)
	assert.Error(t, err)

	//  test length test 97 = True
	var lc gofastogt.LicenseKey
	lc = "32"
	assert.False(t, lc.IsValid())

	lc = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
	assert.True(t, lc.IsValid())

	//  test version by nil
	dataTest = []byte(`{"name":"user", "arch":"user","ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest, &operation)
	assert.Error(t, err)

	//  test version by empty
	dataTest = []byte(`{"name":"user","version":"","arch":"user","ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest, &operation)
	assert.Error(t, err)

	//  test arch by nil
	dataTest = []byte(`{"name":"user","version":"2.0","ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest, &operation)
	assert.Error(t, err)

	//  test arch by empty
	dataTest = []byte(`{"name":"user","version":"2.0","arch":"","ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest, &operation)
	assert.Error(t, err)

	//  test RamTotal by nil
	dataTest = []byte(`{"name":"user","version":"2.0","arch":"user","ram_free":123}`)
	err = json.Unmarshal(dataTest, &operation)
	assert.Error(t, err)

	//  test RamFree by nil
	dataTest = []byte(`{"name":"user","version":"2.0","arch":"user","ram_total":123,}`)
	err = json.Unmarshal(dataTest, &operation)
	assert.Error(t, err)
}
