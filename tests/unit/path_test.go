package unittests

import (
	"os"
	"os/user"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestStableFilePath(t *testing.T) {
	type args struct {
		path string
	}
	usr, err := user.Current()
	if err != nil {
		return
	}
	case1, case3 := filepath.Join(usr.HomeDir, "1.txt"), "/home/sasha/1.txt"
	tests := []struct {
		name    string
		args    args
		want    *string
		wantErr bool
	}{
		{
			name:    "case1",
			args:    args{path: "~/1.txt"},
			want:    &case1,
			wantErr: false,
		},
		{
			name:    "case2",
			args:    args{path: ""},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "case3",
			args:    args{path: "/home/sasha/1.txt"},
			want:    &case3,
			wantErr: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.StableFilePath(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("StableFilePath() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got) 
		})
	}
}

func TestStableDirPath(t *testing.T) {
	type args struct {
		path string
	}
	usr, err := user.Current()
	if err != nil {
		return
	}
	path1, path2, path3, path4, path5 := "~/streamer/", "/streamer", "/home/streamer/", "", "123"
	res1, res2, res3 := filepath.Join(usr.HomeDir, path1[1:])+"/", path2+"/", path3
	tests := []struct {
		name    string
		args    args
		want    *string
		wantErr bool
	}{
		{
			name:    "case_1",
			args:    args{path: path1},
			want:    &res1,
			wantErr: false,
		},
		{
			name:    "case_2",
			args:    args{path: path2},
			want:    &res2,
			wantErr: false,
		},
		{
			name:    "case_3",
			args:    args{path: path3},
			want:    &res3,
			wantErr: false,
		},
		{
			name:    "case_4",
			args:    args{path: path4},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "case_5",
			args:    args{path: path5},
			want:    nil,
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.StableDirPath(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("StableDirPath() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got) 
		})
	}
}

func TestScanFolder(t *testing.T) {
	tempDir := t.TempDir()

	err := os.MkdirAll(filepath.Join(tempDir, "dir1"), 0755)
	assert.NoError(t, err)

	err = os.MkdirAll(filepath.Join(tempDir, "dir2"), 0755)
	assert.NoError(t, err)

	file1, err := os.Create(filepath.Join(tempDir, "file1.mp4"))
	assert.NoError(t, err)
	file1.Close()

	file2, err := os.Create(filepath.Join(tempDir, "dir1", "file2.mp4"))
	assert.NoError(t, err)
	file2.Close()

	file3, err := os.Create(filepath.Join(tempDir, "dir2", "file3.mp4"))
	assert.NoError(t, err)
	file3.Close()

	file4, err := os.Create(filepath.Join(tempDir, "file4.txt"))
	assert.NoError(t, err)
	file4.Close()

	file5, err := os.Create(filepath.Join(tempDir, "dir1", "file5.doc"))
	assert.NoError(t, err)
	file5.Close()

	file6, err := os.Create(filepath.Join(tempDir, "dir2", "file6.png"))
	assert.NoError(t, err)
	file6.Close()

	expectedFilesRecursive := []string{
		filepath.Join(tempDir, "file1.mp4"),
		filepath.Join(tempDir, "dir1", "file2.mp4"),
		filepath.Join(tempDir, "dir2", "file3.mp4"),
	}

	extensions := []string{"ts", "m3u8", "mpd", "mp4", "mkv", "mov", "mp3"}

	actualFilesRecursive, err := gofastogt.ScanFolder(tempDir, extensions, true)
	assert.NoError(t, err)
	assert.ElementsMatch(t, expectedFilesRecursive, actualFilesRecursive)

	expectedFilesNonRecursive := []string{
		filepath.Join(tempDir, "file1.mp4"),
	}

	actualFilesNonRecursive, err := gofastogt.ScanFolder(tempDir, extensions, false)
	assert.NoError(t, err)
	assert.ElementsMatch(t, expectedFilesNonRecursive, actualFilesNonRecursive)

}
