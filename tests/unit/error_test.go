package unittests

import (
	"encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type ExtraError struct {
	gofastogt.ErrorJson
	Data gofastogt.HostAndPort `json:"data"`
}

func (js *ExtraError) UnmarshalJSON(data []byte) error {
	err := js.ErrorJson.UnmarshalJSON(data)
	if err != nil {
		return err
	}

	required := struct {
		Data *gofastogt.HostAndPort `json:"data"`
	}{}
	err = json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Data == nil {
		return errors.New("data field required")
	}

	js.Data = *required.Data
	return nil
}

func TestExtraErrorJson_UnmarshalJSON(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{{
		name: "case_1",
		args: args{data: []byte(`{
			"data":{
				"host":"fastotv.com",
				"port":123
			},
			"code": 2323,
			"message": "Some"
		  }`)},
		wantErr: false,
	},
		{
			name: "case_2",
			args: args{data: []byte(`{
		"message": "awdawdasd"
	  }`)},
			wantErr: true,
		},
		{
			name: "case_3",
			args: args{data: []byte(`{
			"data":{
				"host":"fastotv.com",
				"port":123
			}
	  }`)},
			wantErr: true,
		},
		{
			name:    "case_4",
			args:    args{data: []byte(``)},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		var extra ExtraError
		t.Run(tt.name, func(t *testing.T) {
			err := json.Unmarshal(tt.args.data, &extra)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func TestErrorJson_UnmarshalJSON(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{{
		name: "case_1",
		args: args{data: []byte(`{
			"code": 2323,
			"message": "awdawdasd"
		  }`)},
		wantErr: false,
	},
		{
			name: "case_2",
			args: args{data: []byte(`{
			"message": "awdawdasd"
		  }`)},
			wantErr: true,
		},
		{
			name: "case_3",
			args: args{data: []byte(`{
			"code": 2323
		  }`)},
			wantErr: true,
		},
		{
			name:    "case_4",
			args:    args{data: []byte(``)},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		var provider gofastogt.ErrorJson
		t.Run(tt.name, func(t *testing.T) {
			err := json.Unmarshal(tt.args.data, &provider)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}
