package unittests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestGenerate(t *testing.T) {
	tests := []struct {
		name    string
		count   int
		want    int
		wantErr bool
	}{
		{name: "case_1",
			count:   8,
			want:    8,
			wantErr: false,
		},
		{name: "case_2",
			count:   3,
			want:    3,
			wantErr: false,
		},
		{name: "case_3",
			count:   0,
			want:    0,
			wantErr: true,
		},
		{name: "case_4",
			count:   -1,
			want:    0,
			wantErr: true,
		},
		{name: "case_5",
			count:   16,
			want:    16,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.GenerateString(tt.count)
			if tt.wantErr {
				assert.Error(t, err, "GenerateString() should return an error")
				return
			}
			assert.NoError(t, err, "GenerateString() should not return an error")
			assert.Equal(t, tt.want, len(*got), "len(GenerateString()) = %v, want %v", len(*got), tt.want)
		})
	}
}
