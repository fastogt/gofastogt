package unittests

import (
	"errors"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestLogFileSizeMax(t *testing.T) {
	var maxFileSize int64 = 100
	pathFile, err := gofastogt.StableFilePath("~/test.log")
	assert.NoError(t, err)

	f, err := gofastogt.InitLogFile(*pathFile, maxFileSize)
	assert.NoError(t, err)

	log.SetOutput(f)
	// fills the file more than the maximum
	for i := 0; i != -1; i++ {
		log.Println("Data")
		file, _ := f.Stat()
		if file.Size() > maxFileSize+50 {
			break
		}
	}
	err = f.Close()
	assert.NoError(t, err)

	f, err = gofastogt.InitLogFile(*pathFile, maxFileSize)
	assert.NoError(t, err)

	file, err := f.Stat()
	// checks the newly created file for size
	if file.Size() > maxFileSize {
		err = errors.New("invalid file size")
	}
	assert.NoError(t, err)

	err = f.Close()
	assert.NoError(t, err)

	err = os.Remove(*pathFile)
	assert.NoError(t, err)
}
