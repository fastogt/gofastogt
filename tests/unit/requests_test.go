package unittests

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	protocol "gitlab.com/fastogt/gofastogt/gofastogt/protocol"
)

func TestRequests(t *testing.T) {
	sub := map[string]interface{}{"license_key": "006f68a6d700005eb12ee30000aa54e02c0170455b1af40a70901c4ef60ba0eea82cf50d2044e891c1527e5a9fafd7400"}
	dict := map[string]interface{}{"activate_request": sub}
	raw, _ := json.Marshal(dict)
	params := json.RawMessage(raw)
	req := protocol.NewRequest(protocol.NewStringRPCId("1213"), "ping", &params)
	assert.True(t, req.IsValid())
	assert.Equal(t, req.Params, &params)
	assert.False(t, req.IsNotification())

	notify := protocol.NewNotification("1213", &params)
	assert.True(t, notify.IsValid())
	assert.Equal(t, notify.Params, &params)
	assert.True(t, notify.IsNotification())
}
