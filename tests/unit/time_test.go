package unittests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestIsCurtime(t *testing.T) {
	cur := gofastogt.MakeUTCTimestamp()
	tm := gofastogt.UtcTime2Time(cur)
	assert.Equal(t, gofastogt.Time2UtcTimeMsec(tm), cur)
}
