package unittests

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestMakeHostAndPortFromString(t *testing.T) {
	tests := []struct {
		name    string
		data    string
		want    *gofastogt.HostAndPort
		wantErr bool
	}{
		{name: "case_1",
			data:    "fastotv.com:15433",
			want:    &gofastogt.HostAndPort{Host: "fastotv.com", Port: 15433},
			wantErr: false,
		},
		{name: "case_2",
			data:    "localhost:15433",
			want:    &gofastogt.HostAndPort{Host: "localhost", Port: 15433},
			wantErr: false,
		},
		{name: "case_3",
			data:    "fastocloud-pro:15433",
			want:    &gofastogt.HostAndPort{Host: "fastocloud-pro", Port: 15433},
			wantErr: false,
		},
		{name: "case_4",
			data:    "fastocloud_pro:15433",
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.MakeHostAndPortFromString(tt.data)
			assert.Equal(t, tt.wantErr, err != nil)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestMakeHostAndPortFromUrl(t *testing.T) {
	tests := []struct {
		name    string
		data    string
		want    *gofastogt.HostAndPort
		wantErr bool
	}{{name: "case_1",
		data:    "https://example.com",
		want:    &gofastogt.HostAndPort{Host: "example.com", Port: 443},
		wantErr: false,
	},
		{name: "case_2",
			data:    "http://example.com",
			want:    &gofastogt.HostAndPort{Host: "example.com", Port: 80},
			wantErr: false,
		},
		{name: "case_3",
			data:    "http://127.0.0.1:8080",
			want:    &gofastogt.HostAndPort{Host: "127.0.0.1", Port: 8080},
			wantErr: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.MakeHostAndPortFromUrl(tt.data)
			assert.Equal(t, tt.wantErr, err != nil)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestGetIPFromRequest(t *testing.T) {
	type args struct {
		r *http.Request
	}
	ip := "192.167.1.1"
	ipv6 := "::ffff:127.0.0.1"
	port := uint16(8080)

	xhead := http.Header{}
	xhead.Add("X-Real-Ip", ip)
	req2 := http.Request{Header: xhead}

	xheadv6 := http.Header{}
	xheadv6.Add("X-Real-Ip", ipv6)
	req2v6 := http.Request{Header: xheadv6}

	xfor := http.Header{}
	xfor.Add("X-Forwarded-For", ip)
	req3 := http.Request{Header: xfor}

	xfor2 := http.Header{}
	xfor2.Add("X-Forwarded-For", fmt.Sprintf("%v, fastotv.com", ip))
	req4 := http.Request{Header: xfor2}

	xforv6 := http.Header{}
	xforv6.Add("X-Forwarded-For", ipv6)
	req3v6 := http.Request{Header: xforv6}

	xfor2v6 := http.Header{}
	xfor2v6.Add("X-Forwarded-For", fmt.Sprintf("%v, fastotv.com", ipv6))
	req4v6 := http.Request{Header: xfor2v6}

	tests := []struct {
		name    string
		args    args
		ip      *string
		port    *uint16
		wantErr bool
	}{
		{
			name:    "case_1_v4",
			args:    args{r: &http.Request{RemoteAddr: fmt.Sprintf("%v:%v", ip, port)}},
			ip:      &ip,
			port:    &port,
			wantErr: false,
		},
		{
			name:    "case_2_v4",
			args:    args{r: &req2},
			ip:      &ip,
			port:    nil,
			wantErr: false,
		},
		{
			name:    "case_3_v4",
			args:    args{r: &req3},
			ip:      &ip,
			port:    nil,
			wantErr: false,
		},
		{
			name:    "case_4_v4",
			args:    args{r: &req4},
			ip:      &ip,
			port:    nil,
			wantErr: false,
		},
		{
			name:    "case_1_v6",
			args:    args{r: &http.Request{RemoteAddr: fmt.Sprintf("[%v]:%v", ipv6, port)}},
			ip:      &ipv6,
			port:    &port,
			wantErr: false,
		},
		{
			name:    "case_2_v6",
			args:    args{r: &req2v6},
			ip:      &ipv6,
			port:    nil,
			wantErr: false,
		},
		{
			name:    "case_3_v6",
			args:    args{r: &req3v6},
			ip:      &ipv6,
			port:    nil,
			wantErr: false,
		},
		{
			name:    "case_4_v6",
			args:    args{r: &req4v6},
			ip:      &ipv6,
			port:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ip, port, err := gofastogt.GetIPFromRequest(tt.args.r)
			assert.Equal(t, tt.wantErr, err != nil)
			assert.Equal(t, tt.ip, ip)
			assert.Equal(t, tt.port, port)
		})
	}
}

func TestIsHost(t *testing.T) {
	tests := []struct {
		name      string
		args      string
		assertion bool
	}{
		{name: "case_1",
			args:      "fastocloud.com",
			assertion: true},
		{name: "case_2",
			args:      "fastocloud-pro",
			assertion: true},
		{name: "case_3",
			args:      "fastocloud",
			assertion: true},
		{name: "case_4",
			args:      "localhost",
			assertion: true},
		{name: "case_5",
			args:      "sub.fastocloud.com",
			assertion: true},
		{name: "case_6",
			args:      "com@fastocloud.com",
			assertion: false},
		{name: "case_7",
			args:      "fastocloud@com",
			assertion: false},
		{name: "case_8",
			args:      "255.255.255.255",
			assertion: false},
		{name: "case_9",
			args:      "fastocloud_pro",
			assertion: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := gofastogt.IsHost(tt.args)
			assert.Equal(t, tt.assertion, got)
		})
	}
}

func TestIsIP(t *testing.T) {
	tests := []struct {
		name   string
		ip     string
		result bool
	}{
		{name: "case_1",
			ip:     "127.0.0.1",
			result: true},
		{name: "case_2",
			ip:     "255.255.255.256",
			result: false},
		{name: "case_3",
			ip:     "::ffff:127.0.0.1",
			result: true},
		{name: "case_4",
			ip:     "::1",
			result: true},
		{name: "case_5",
			ip:     "::ffff:127.0.0.1111",
			result: false},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.result, gofastogt.IsIP(tt.ip))
		})
	}
}

func TestIsIPOrHost(t *testing.T) {
	tests := []struct {
		name   string
		ip     string
		result bool
	}{
		{name: "case_1",
			ip:     "127.0.0.1",
			result: true},
		{name: "case_2",
			ip:     "255.255.255.256",
			result: false},
		{name: "case_3",
			ip:     "::ffff:127.0.0.1",
			result: true},
		{name: "case_3",
			ip:     "::1",
			result: true},
		{
			name:   "case_4",
			ip:     "::ffff:127.0.0.1111",
			result: false},
		{name: "case_5",
			ip:     "sub.fastocloud.com",
			result: true},
		{name: "case_6",
			ip:     "fastocloud",
			result: true},
		{name: "case_6",
			ip:     "fastocloud@com",
			result: false},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.result, gofastogt.IsIPOrHost(tt.ip))
		})
	}
}

func TestToStringHexFromBytes(t *testing.T) {
	tests := []struct {
		name   string
		text   []byte
		result string
	}{
		{
			name:   "case_1",
			text:   []byte("example"),
			result: "6578616d706c65",
		},
		{
			name:   "case_2",
			text:   []byte("test example"),
			result: "74657374206578616d706c65",
		},
		{
			name:   "case_3",
			text:   []byte("123test/exa@mple"),
			result: "313233746573742f657861406d706c65",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.result, gofastogt.ToStringHexFromBytes(tt.text))
		})
	}
}

func TestToBytesFromStringHex(t *testing.T) {
	tests := []struct {
		name   string
		text   string
		result []byte
	}{
		{
			name:   "case_1",
			text:   "6578616d706c65",
			result: []byte("example"),
		},
		{
			name:   "case_2",
			text:   "74657374206578616d706c65",
			result: []byte("test example"),
		},
		{
			name:   "case_3",
			text:   "313233746573742f657861406d706c65",
			result: []byte("123test/exa@mple"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h, err := gofastogt.ToBytesFromStringHex(tt.text)
			assert.NoError(t, err)
			assert.Equal(t, tt.result, h)
		})
	}
}

func TestHttpsConfig(t *testing.T) {
	testVariable4 := gofastogt.HttpsConfig{}
	testJSON := []byte(`{"key":"key_path", "cert": "cert_path"}`)
	err := json.Unmarshal(testJSON, &testVariable4)
	assert.NoError(t, err)
	assert.Equal(t, testVariable4.Key, "key_path")
	assert.Equal(t, testVariable4.Cert, "cert_path")

	testCases4 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil key",
			testArray: []byte(`{"key":"", "cert":"cert_path"}`),
		},
		{
			name:      "nil cert",
			testArray: []byte(`{"key":"", "cert": ""}`),
		},
		{
			name:      "nil data",
			testArray: []byte(`{}`),
		},
	}

	for _, obj := range testCases4 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable4))
		})
	}
}
