package unittests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestIsRunningInDocker(t *testing.T) {
	assert.True(t, gofastogt.IsRunningInDocker())
}
